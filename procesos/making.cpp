/*ppid-> id del proceso padre, pid-> id proceso hijo*/

#include <stdio.h>
#include <unistd.h>
int main()
{
	printf("El ID proceso es %i\n", (int) getpid());
	printf("El ID del proceso padre(shell) es %i\n", (int) getppid());
	return 0;
}


