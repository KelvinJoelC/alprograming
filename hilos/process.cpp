#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
/* fork duplica el programa que estas utilizando actualmente
 * exec tranforma el proceso en una instancia del programa deseado*/

int spawn (char* programa, char** arg_list){
	pid_t child_pid;  	/*Se guarda la ID del proceso*/
	child_pid = fork();     /*fork divide duplica el programa y  child_pid == 0 solo en el HIJO, 
				  el padre recibe valor de retorno el valor de la ID del hijo*/

	/*Comienza a ejecutarse los dos programas  a la vez*/

	if(child_pid !=0){		/*Si es el padre..*/
		return child_pid; 	/*Devuelve la ID del hijo*/
	}else{
	execvp (programa, arg_list) ;
		/*Le pasas el programa que quieres que ejecute el hijo, y los argumentos*/
	fprintf(stderr, "Se modificó mal el programa hijo");
	abort();
		/*Solo si el execvp no se realiza se ejecuta el codigo de abajo*/
	}

}

int main(){

	pid_t child_pid;

	int child_status;
	
	char* arg_list[] = {(char*) "./hijo", NULL};  
	/* arg_list[0] nombre del programa
	 * NULL tiene que terminar la lista en NULL obligatoriamente*/
	child_pid = spawn ((char*) "./hijo", arg_list); 
	/*Llamamos a la funcion spawn y devuelve la ID del hijo*/

	for(int i=0; i<3;i++){  /*El número de señales a ejecutarse */
		sleep(1);
		kill(child_pid, SIGUSR1);
		printf(" %i señales enviadas\n",i+1);	
	}

	wait(&child_status); /* Bloquea el proceso de llamada hasta que 
			        uno de sus procesos secundarios salga (o se produzca un error)*/

	if(WIFEXITED (child_status))
		printf("El proceso hijo a salido correctamente\n");
	else
		printf("El proceso hijo a salido incorrectamente\n");

	return EXIT_SUCCESS;


}

