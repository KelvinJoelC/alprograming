#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

int spawn(char* programa, char** arg_programa){
	pid_t child_pid;
	child_pid = fork();


	if(child_pid !=0){
		return child_pid;
	}else{
		execvp(programa, arg_programa);
		fprintf(stderr, "error al modificar el programa hijo");
		abort();
	}
}
int main(){

	pid_t child_pid;
	int child_status;
	char* arg_list[] = {(char *)"./hijo2",NULL };
	child_pid = spawn((char*)"./hijo2",arg_list);

	for(int i=0; i<3;i++)
	{
		kill(child_pid,SIGUSR1);
		sleep(1);
	}

	wait(&child_status);

	if(WIFEXITED(child_status))
		printf("correcto");
	else
		printf("correcto");



return EXIT_SUCCESS;
}
