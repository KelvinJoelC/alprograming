#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

sig_atomic_t contador=0;

void handler(int numeroSenales){
	++contador;

}


int main(){

	struct sigaction sa;
	memset(&sa, 0, sizeof(sa));
	sa.sa_handler = &handler;
	sigaction(SIGUSR1,&sa,NULL);

	while(contador<3){
		sleep(1);
	}
	return 0;


}


