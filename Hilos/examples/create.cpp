#include <pthread.h>
#include <stdio.h>
#include <unistd.h>

/*Imprime por pantalla 1, no recibe parametros ni retorna nada*/
void* print_1 (void* unused)
{
	while (1){
		printf("1");
		sleep(7/8);
	}
	return NULL;
}
/* Programa principal*/
int main ()
{
	pthread_t thread_id; //Variable tread_id de tipo pthread_t, guarda la id del nuevo hilo
	
	//Crea y ejecuta en un hilo nuevo la funcion print_1 
	pthread_create (&thread_id, NULL, &print_1, NULL);
	
	while (1){
	//El programa main continua e imprime 0
		printf("0");
		sleep(7/8);
		}
	return 0;
}
