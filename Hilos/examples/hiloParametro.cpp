#include <pthread.h>
#include <stdio.h>
struct  parametroImprimirCaracter /* Parametros de la funcion */
{
	char caracter; 	/* El caracter para imprimir. */
	int contador;   /* El numero de veces que lo va a imprimir.*/
};

void* char_print (void* parameters) /*Imprime un numero de caracteres recibidos por parametros*/
{
	struct parametroImprimirCaracter* p = (struct parametroImprimirCaracter*) parameters;
	int i;
	for (i = 0; i < p->count; ++i)
		printf(" %c" p->caracter);
	return NULL; //Null es un cero como direccion de memoria.
}


int main ()
{
	struct parametroImprimirCaracter sthread1_args;
	struct parametroImprimirCaracter thread2_args;
	
	pthread_t thread1_id;
	pthread_t thread2_id;

	thread1_args.caracter = 'x';
	thread1_args.contador = 30000;
	pthread_create (&thread1_id, NULL, &char_print, &thread1_args); /* Create a new thread to print 30,000 ’x’s. */

	thread2_args.caracter = 'o';
	thread2_args.contador = 20000;
	pthread_create (&thread2_id, NULL, &char_print, &thread2_args); /* Create a new thread to print 20,000 o’s. */
	
	return 0;
}
